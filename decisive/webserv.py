import os

import eventlet

from flask import Flask, request
from flask_redis import FlaskRedis
from flask_socketio import SocketIO, send
from decisive.applogic import message_handler

app = Flask(__name__)

app.config['REDIS_URL'] = os.getenv('REDIS_URL', None)
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY', None)

eventlet.monkey_patch()

cache = FlaskRedis(app)
websocket = SocketIO(app, message_queue=app.config['REDIS_URL'])


@websocket.on('message')
def socket_callback(message):
    resp = message_handler(cache, request.sid, message)
    if resp:
        for l in resp.splitlines():
            websocket.sleep(1)
            send(l)
