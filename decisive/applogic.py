import os
import re
import random

TTL = int(os.getenv('CACHE_TTL', '1800'))


def message_handler(cache, user_id, message):
    response = None

    stage = cache.get(user_id+':stage')
    if stage:
        stage = stage.decode()
    else:
        stage = 'none'

    if stage in HANDLERS.keys() and message != 'debug':
        response = HANDLERS[stage](cache, user_id, message)
    else:
        response = ("Sorry, something went wrong.\n"
                    "Tell the developer that my stage with user {} is {}"
                    ).format(user_id, stage)
    return response


def onboarding(cache, user_id, _):
    cache.set(user_id+':stage', 'introduced', ex=7200)
    return 'Hello! What decision are you trying to make today?'


def set_decision(cache, user_id, _):
    cache.set(user_id+':stage', 'decision_set', ex=7200)
    return ("I can help you with that!\n"
            "What are some possible options to choose from?\n"
            "When you're finished, just say 'that's all'")


def list_options(cache, user_id, message):
    if message.replace("'", '').lower().strip() == 'thats all':
        return filter_options(cache, user_id, message)
    cache.lpush(user_id+':options', message)
    return None


def filter_options(cache, user_id, _):
    options = list(set(cache.lrange(user_id+':options', 0, -1)))
    option_text = '\n'.join(["Option #{}: {}".format(i, o.decode())
                             for i, o in
                             zip(range(1, 1+len(options)), options)])
    cache.set(user_id+':stage', 'options_set', ex=7200)
    return ("Right now the options are:\n"
            "{}"
            "\n"
            "Which ones do you still want to choose from?"
            ).format(option_text)


def make_decision(cache, user_id, message):
    options = list(set(cache.lrange(user_id+':options', 0, -1)))
    if message.strip() == 'all':
        remaining = range(1, 1+len(options))
    else:
        # Parses whitespace or comma seperated numbers, then range checks them.
        remaining = [int(x) for x in re.split('[^0-9]', message)
                     if x.strip().isdigit() and int(x) < 1+len(options)]

    if len(remaining) > 1:
        options = [o for i, o in zip(range(1, 1+len(options)), options)
                   if i in remaining]
        cache.delete(user_id+':options')
        for opt in options:
            cache.lpush(user_id+':options', opt)
        cache.set(user_id+':stage', 'decision_ready', ex=7200)
        option_text = '\n'.join(["Option #{}: {}".format(i, o.decode())
                                 for i, o in
                                 zip(range(1, 1+len(options)), options)])
        rand_index = random.randrange(0, len(options))
        choice = options[rand_index].decode()
        cache.set(user_id+':choice', choice, ex=7200)
        return ("Alright! Now the options are:\n"
                "{}"
                "\n"
                "Would you be okay with choosing Option #{}: {}?"
                ).format(option_text, rand_index+1, choice)
    if len(remaining) == 1:
        choice = options[remaining[0]-1].decode()
        cache.set(user_id+':choice', choice, ex=7200)
        cache.delete(user_id+':stage')
        cache.delete(user_id+':options')
        cache.delete(user_id+':choice')
        return "Awesome! Sounds like you've decided on {}.".format(choice)
    return "Sorry, I didn't understand that. Try again."


def read_response(cache, user_id, message):
    choice = cache.get(user_id+':choice').decode()
    if message[0].lower() == 'n':
        # If it's not okay, don't ask again.
        cache.lrem(user_id+':options', 1, choice)
        opt_count = cache.llen(user_id+':options')
        if opt_count == 1:
            choice = cache.lpop(user_id+':options').decode()
            # Process of elimination
            cache.delete(user_id+':stage')
            cache.delete(user_id+':options')
            cache.delete(user_id+':choice')
            return "Awesome! Sounds like you've decided on {}.".format(choice)
        return filter_options(cache, user_id, message)
    if message[0].lower() == 'y':
        # If it is okay, then that's all that's needed.
        cache.delete(user_id+':stage')
        cache.delete(user_id+':options')
        cache.delete(user_id+':choice')
        return "Awesome! Sounds like you've decided on {}.".format(choice)
    return "Sorry. I'm not sure what you mean. Yes or No?"


HANDLERS = {
    'none': onboarding,
    'introduced': set_decision,
    'decision_set': list_options,
    'options_set': make_decision,
    'decision_ready': read_response,
}
